public class SingletonCalculator {
	private static SingletonCalculator instance = null;

	private SingletonCalculator() {
	}

	public int add(int i, int j, int k) {
		int result = i + j + k;
		return result;
	}

	public static SingletonCalculator getInstance() {
		// TODO Auto-generated method stub
		if (instance == null) {
			instance = new SingletonCalculator();
		}
		return instance;
	}

}